.. -*- mode: rst; coding: utf-8 -*-

============
DevOps
============


:Authors:
     Jack

:Version: 1.0.1

.. contents:: Table of Contents
  :backlinks: top


Basic run
===============
run::

    celery -A automation worker -l INFO --autoscale=4,1 \
    --autoreload --time-limit=1000 -n mitra -Q runscript

    docker run -it --link some-redis:redis --rm redis redis-cli -h redis -p 6379

    docker run -it --rm redis redis-cli -h 192.168.117.34 -p 6379

===================
Deployment Steps
===================

前端
===========

#. 本地搭建nodejs环境 版本:v8.11.3+，nginx或者apache服务

#. 修改配置文件 `/automation-frontend/src/common/HttpService.tsx`

#. 打包文件

    From within your environment, just run::
    
            $  npm run build

#. 把build目录下的文件全部copy到 `/var/www/html` 目录下

.. Note::
    | 建议直接让前端打一个生产部署的前端包


后端
===========
服务运行流程
------------

#. 创建python环境

    From within your environment, just run::
    
            $  virtualenv deploy-test-env

#. 解压文件 ``dep-packs.tar.gz`` 到后端运行目录里面

    From within your environment, just run::
    
            $  pip install --no-index -f /home/jack/git/automation/dep-packs -r requirements.txt


#. 运行项目后端

    From within your environment, just run::

        $ nohup python manage.py runserver 0:8000 --noreload &

#. 运行任务队列

    From within your environment, just run::

        $ nohup celery worker -A automation --autoscale=10,2 \
        --maxtasksperchild=100 --time-limit=1800 -l info --logfile="~/%n%I.log" -n dev-test &

.. Note::
    | 配置目前还需改python源码文件 `settings.py`, `celeryconfig.py`, **后面我们会改成无需改源码文件的方式**
