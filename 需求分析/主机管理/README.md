# 主机管理

## 脑图

![脑图](E:\VM_Share\naotu.png)

## 主要api接口

- 主机列表
- 主机详情

### 主机列表可查询项

- IP地址
- 主机名称
- 网络域
- 机房地址
- 主机组

## 功能点

- 主机的增删查改 （5 D）
- 主机组管理 （4 D）
- 主机监控信息（磁盘， cpu， 内存） （3 D）
- 可導出導入 主机信息 yml文件 （5 D）
- rest api 接口 提供 常用 命令功能 （7 D）
  - 开关机
- websocket 接口提供 主机 可用用户 shell 交互 接口 （10 D）

## DataDict

2018-10-09 15:55:41 星期二
负责人： 王智杰

-  主机表（host_info_basic），储存主机信息

|字段|类型|constraint|空|默认|注释|
|:----    |:-|:-------    |:--- |-- -|------      |--|
|id    |int  | 主键 |否 |  |             |
|ip |char(50) ||否 |    |   ip地址  |
|port |mediumint ||否 |   22 |   ssh端口  |
|hostname |char(50) ||否   |    |   主机名称    |
|domain     |char(128) ||是   |    |    网络域     |
|address |int | 外键|是   |   |   机房地址    |
|cpu |int   | |是   | 0  |   CPU(核)    |
|memory |int   ||是   | 0  |   内存(G)    |
|disk |int   |  |是   | 0  |   容量(G)    |
|note | tinytext    | |是   | 0  |    备注   |
|create_user |int   | 外键 |否   | 0  |   创建者    |
|create_time |timestamp   |  |否   | current_timestamp |   创建时间    |
|update_user |int   | 外键 |否   | 0  |   跟新者    |
|update_type |tinyint  |  |否   | 0  |   更新类型    |
|update_time |timestamp  |  |否   | current_timestamp |   更新时间 时间戳   |

- 备注：无

--------

-  主机组表（host_group），储存主机信息

|字段|类型|constraint|空|默认|注释|
|:----    |:-|:-------    |:--- |-- -|------      |--|
|id    |int  | 主键 |否 |  |             |
|name |char(125) ||否   |    |   名称    |
|note | tinytext    | |是   | 0  |    备注   |
|create_user |int   | 外键 |否   |   |   创建者    |
|create_time |timestamp   |  |否   |current_timestamp  |   创建时间    |
|update_user |int   | 外键 |否   |  |   跟新者    |
|update_time |timestamp  |  |否   |current_timestamp |   更新时间 时间戳   |

- 备注：无

-  主机地址（host_address），储存主机信息

|字段|类型|constraint|空|默认|注释|
|:----    |:-|:-------    |:--- |-- -|------      |--|
|id    |int  | 主键 |否 |  |             |
|name |char(125) ||否   |    |   名称    |
|create_user |int   | 外键 |否   |   |   创建者    |
|create_time |timestamp   |  |否   |current_timestamp  |   创建时间    |
|update_user |int   | 外键 |否   |  |   跟新者    |
|update_time |timestamp  |  |否   |current_timestamp |   更新时间 时间戳   |

- 备注：无


## API 接口

![API 接口](E:\VM_Share\api.png)
